import './fe-menu-link.scss';

const FeMenuLink = (props) => {
  const { icon, text, onClick } = props;
  return (
    <div className="menu-link" onClick={onClick}>
      {icon}
      <span className="menu-link_text">{text}</span>
    </div>
  )
}

export default FeMenuLink;
