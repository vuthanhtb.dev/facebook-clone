/* eslint-disable jsx-a11y/img-redundant-alt */
import {
  ChatBubbleOutline,
  Favorite,
  MoreVert,
  ThumbUp,
  ThumbUpAltOutlined,
  ShareOutlined
} from '@mui/icons-material';
import IconButton from '@mui/material/IconButton';
import './fe-post.scss';

const FePost = (props) => {
  const {
    profile,
    username,
    date,
    body,
    photo,
    like,
    comment
  } = props.post;
  return (
    <div className="post">
      <div className="post_wrapper">
        <div className="post_top">
          <div className="post_top__left">
            <img src={profile} alt="profile" className="post_top__left_img" />
            <span className="post_top__left__name">{username}</span>
            <span className="post_top__left__date">{date}</span>
          </div>
          <div className="post_top__right">
            <IconButton>
              <MoreVert className="post_top__right__button"/>
            </IconButton>
          </div>
        </div>
        <div className="post_center">
          <span className="post_center_text">{body}</span>
          <img
            src={photo}
            alt="photo"
            className="post_center_img"
          />
        </div>
        <div className="post_bottom">
          <div className="post_bottom__left">
            <Favorite
              className="post_bottom__left__icon"
              style={{ color: "red" }}
            />
            <ThumbUp
              className="post_bottom__left__icon"
              style={{ color: "#011631" }}
            />
            <span className="post_bottom__left__like-counter">{like}</span>
          </div>
          <div className="post_bottom__right">
            <span className="post_bottom__right__comment">{comment} • comments • share</span>
          </div>
        </div>
        <hr className="post_hr" />
        <div className="post_footer">
          <div className="post_footer__item">
            <ThumbUpAltOutlined className="post_footer__item__icon"/>
            <span className="post_footer__item__text">Like</span>
          </div>
          <div className="post_footer__item">
            <ChatBubbleOutline className="post_footer__item__icon"/>
            <span className="post_footer__item__text">Comment</span>
          </div>
          <div className="post_footer__item">
            <ShareOutlined className="post_footer__item__icon"/>
            <span className="post_footer__item__text">Share</span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FePost;
