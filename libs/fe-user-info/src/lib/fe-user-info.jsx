import './fe-user-info.scss';

const FeUserInfo = (props) => {
  const { name, avatar } = props;
  return (
    <div className="friend">
      <li className="friend_item">
        <img
          src={avatar}
          alt="friend"
          className="friend_item__img"
        />
        <span className="friend_item__name">{name}</span>
      </li>
    </div>
  );
}

export default FeUserInfo;
