import VideoCameraFrontIcon from '@mui/icons-material/VideoCameraFront';
import PermMediaIcon from '@mui/icons-material/PermMedia';
import EmojiEmotionsIcon from '@mui/icons-material/EmojiEmotions';
import './fe-share.scss';

const FeShare = (props) => {
  const { picture, name } = props.currentUser;
  return (
    <div className="share">
      <div className="share-wrapper">
        <div className="share-wrapper_top">
          <img src={picture} alt="profile" className="share-wrapper_top__profile" />
          <input
            type="text"
            className="share-wrapper_top__input"
            placeholder={`${name} ơi, bạn đang nghĩ gì thế?`}
          />
        </div>
        <hr className="share-wrapper_hr" />
        <div className="share-wrapper_bottom">
          <div className="share-wrapper_buttom__options">
            <div className="share-wrapper_buttom__options__item">
              <VideoCameraFrontIcon
                className="share-wrapper_buttom__options__item__icon"
                style={{ color: "#BB0000F2" }}
              />
              <span className="share-wrapper_buttom__options__item__text">Live Video</span>
            </div>
            <div className="share-wrapper_buttom__options__item">
              <PermMediaIcon
                className="share-wrapper_buttom__options__item__icon"
                style={{ color: "#2E0196F1" }}
              />
              <span className="share-wrapper_buttom__options__item__text">Photo/Video</span>
            </div>
            <div className="share-wrapper_buttom__options__item">
              <EmojiEmotionsIcon
                className="share-wrapper_buttom__options__item__icon"
                style={{ color: "#BFC600EC" }}
              />
              <span className="share-wrapper_buttom__options__item__text">Feelings/Activity</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FeShare;
