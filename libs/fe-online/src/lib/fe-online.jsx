import './fe-online.scss';

const FeOnline = (props) => {
  const { name, picture } = props.user;
  return (
    <div className="online">
      <li className="online_friends">
        <div className="online_friends__img">
          <img
            src={picture}
            alt="profile"
            className="online_friends__img__item"
          />
          <span className="online_friends__img__online"></span>
        </div>
        <span className="online_friends__name">{name}</span>
      </li>
    </div>
  );
}
export default FeOnline;
