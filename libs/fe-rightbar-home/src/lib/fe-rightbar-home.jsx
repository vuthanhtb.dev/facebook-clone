import { FeOnline } from '@lib/fe-online';
import './fe-rightbar-home.scss';

const FeRightbarHome = (props) => {
  const { users } = props;

  return (
    <div className="rightbar-home">
      <div className="rightbar-home_birhtday">
        <img
          src="https://cdn3d.iconscout.com/3d/premium/thumb/birthday-gift-5646524-4705643.png"
          alt="birhtday"
          className="rightbar-home_birhtday__img"
        />
        <span className="rightbar-home_birhtday__text">
          <b>Tiểu Tiểu</b> and <b>other friends</b> have a birhtday today
        </span>
      </div>
      <img
        src="https://digitalschoolofmarketing.co.za/wp-content/uploads/2020/05/shutterstock_163794281-e1590582031964.png"
        alt="advert"
        className="rightbar-home_advertising"
      />
      <span className="rightbar-home_title">Online Friends</span>
      <ul className="rightbar-home_friends">
        {
          users.map((user) => (
            <FeOnline
              key={user.id}
              user={user}
            />
          ))
        }
      </ul>
    </div>
  );
}

export default FeRightbarHome;
