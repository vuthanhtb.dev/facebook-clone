import { FeStoryCard } from '@lib/fe-story-card';
import './fe-stories.scss';

const FeStories = (props) => {
  const {
    currentUser: {
      picture,
      name
    },
    friends
  } = props;
  return (
    <div className="stories">
      <FeStoryCard
        profile={picture}
        name={name}
        isUserCurrent
      />
      {
        friends.map((friend) => (
          <FeStoryCard
            profile={friend.profilePicture}
            name={friend.name}
          />
        ))
      }
    </div>
  );
}

export default FeStories;
