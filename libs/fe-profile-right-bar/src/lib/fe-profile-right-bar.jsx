import { useNavigate } from 'react-router-dom';
import './fe-profile-right-bar.scss';

const FeProfileRightbar = (props) => {
  const {
    currentUser: {
      email,
      phoneNumber,
      address,
      country,
      relationship,
    },
    friendsClose
  } = props;

  const navigate = useNavigate();
  const navigateEditProfile = () => navigate('/edit-profile');

  return (
    <div className="profile-rightbar">
      <div className="profile-rightbar_heading">
        <span className="profile-rightbr__heading_title">User Information</span>
        <span className="profile-rightbr__heading_edit-button" onClick={navigateEditProfile}>Edit Profile</span>
      </div>

      <div className="profile-rightbar_info">
        <div className="profile-rightbar_info__item">
          <span className="profile-rightbar_info__item__key">Email:</span>
          <span className="profile-rightbar_info__item__value">{email}</span>
        </div>
        <div className="profile-rightbar_info__item">
          <span className="profile-rightbar_info__item__key">Phone Number:</span>
          <span className="profile-rightbar_info__item__value">{phoneNumber}</span>
        </div>
        <div className="profile-rightbar_info__item">
          <span className="profile-rightbar_info__item__key">Address:</span>
          <span className="profile-rightbar_info__item__value">{address}</span>
        </div>
        <div className="profile-rightbar_info__item">
          <span className="profile-rightbar_info__item__key">Country:</span>
          <span className="profile-rightbar_info__item__value">{country}</span>
        </div>
        <div className="profile-rightbar_info__item">
          <span className="profile-rightbar_info__item__key">Relationship:</span>
          <span className="profile-rightbar_info__item__value">{relationship}</span>
        </div>
      </div>

      <h4 className="profile-rightbar_title">Close Friends</h4>
      <div className="profile-rightbar_following">
        {
          friendsClose.map((friend) => (
            <div className="profile-rightbar_following__item">
              <img
                src={friend.profilePicture}
                alt="following"
                className="profile-rightbar_following__item__img"
              />
              <span className="profile-rightbar_following__item__name">{friend.name}</span>
            </div>
          ))
        }
      </div>
    </div>
  );
}

export default FeProfileRightbar;
