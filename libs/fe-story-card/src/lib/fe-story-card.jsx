import './fe-story-card.scss';

const FeStoryCard = (props) => {
  const { profile, name, isUserCurrent } = props;
  return (
    <div className="story-card">
      <div className="story-card_overlay"></div>
      <img
        src={profile}
        alt="profile"
        className="story-card_profile"
      />
      <img
        src={profile}
        alt="background"
        className="story-card_background"
      />
      {
        isUserCurrent && (
          <img
            src="https://www.freeiconspng.com/thumbs/add-icon-png/add-1-icon--flatastic-1-iconset--custom-icon-design-0.png"
            alt="add"
            className="story-add"
          />
        )
      }
      <span className="story-card_text">{name}</span>
    </div>
  );
}

export default FeStoryCard;
