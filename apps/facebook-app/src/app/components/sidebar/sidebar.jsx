import { useNavigate } from 'react-router-dom';
import RssFeedIcon from '@mui/icons-material/RssFeed';
import ChatIcon from '@mui/icons-material/Chat';
import VideocamIcon from '@mui/icons-material/Videocam';
import GroupsIcon from '@mui/icons-material/Groups';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import EventIcon from '@mui/icons-material/Event';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import ExitToAppOutlinedIcon from '@mui/icons-material/ExitToAppOutlined';

import { FeUserInfo } from '@lib/fe-user-info';
import { FeMenuLink } from '@lib/fe-menu-link';

import './sidebar.scss';


const Sidebar = (props) => {
  const { friends, currentUser } = props;
  const navigate = useNavigate();
  const navigateLogin = () => navigate('/login');

  return (
    <div className="sidebar-container">
      <div className="sidebar-wrapper">
        <FeUserInfo
          avatar={currentUser.picture}
          name={currentUser.name}
        />
        <FeMenuLink icon={<RssFeedIcon />} text="Feed" />
        <FeMenuLink icon={<ChatIcon />} text="Chats" />
        <FeMenuLink icon={<VideocamIcon />} text="Videos" />
        <FeMenuLink icon={<GroupsIcon />} text="Friends" />
        <FeMenuLink icon={<BookmarkIcon />} text="Bookmarks" />
        <FeMenuLink icon={<ShoppingCartIcon />} text="Marketplace" />
        <FeMenuLink icon={<EventIcon />} text="Events" />
        <FeMenuLink icon={<Brightness4Icon />} text="Theme" />
        <FeMenuLink icon={<ExitToAppOutlinedIcon />} text="Logout" onClick={navigateLogin}/>

        <button className="sidebar-button">Show More</button>
        <hr className="sidebar-hr" />

        <ul className="sidebar-friends">
          {
            friends.map((friend) => (
              <FeUserInfo
                key={friend.id}
                avatar={friend.profilePicture}
                name={friend.name}
              />
            ))
          }
          
        </ul>
      </div>
    </div>
  );
}

export default Sidebar;
