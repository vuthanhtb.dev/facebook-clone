import { FeStories } from '@lib/fe-stories';
import { FeShare } from '@lib/fe-share';
import { FePost } from '@lib/fe-post';
import './feed.scss';

const Feed = (props) => {
  const { friends, posts, currentUser } = props;
  return (
    <div className="feed">
      <div className="feed-wrapper">
        <FeStories friends={friends} currentUser={currentUser} />
        <FeShare currentUser={currentUser} />
        {
          posts.map((post) => (
            <FePost key={post.id} post={post}/>
          ))
        }
      </div>
    </div>
  );
}

export default Feed;
