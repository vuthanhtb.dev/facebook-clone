/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import { FeRightbarHome } from '@lib/fe-rightbar-home';
import { FeProfileRightbar } from '@lib/fe-profile-right-bar';
import './rightbar.scss';

const Rightbar = (props) => {
  const { friendsOnline, currentUser, friendsClose } = props;
  return (
    <div className="rightbar">
      <div className="rightbar-wrapper">
        {
          props.isProfile
            ? (<FeProfileRightbar currentUser={currentUser} friendsClose={friendsClose}/>)
            : (<FeRightbarHome users={friendsOnline} />)
        }
        
      </div>
    </div>
  );
}

export default Rightbar;
