import { useNavigate } from 'react-router-dom';
import SearchIcon from '@mui/icons-material/Search';
import PersonIcon from '@mui/icons-material/Person';
import ChatBubbleIcon from '@mui/icons-material/ChatBubble';
import NotificationsIcon from '@mui/icons-material/Notifications';

import './navbar.scss';

const Navbar = (props) => {
  const { picture, messageCount, notificationCount, addFriendCount} = props.currentUser;
  const navigate = useNavigate();

  const navigateHome = () => navigate('/');
  const navigateProfile = () => navigate('/profile');

  return (
    <div className="navbar-container">
      <div className="navbar-container_left">
        <span className="logo" onClick={navigateHome}>Facebook</span>
      </div>
      <div className="navbar-container_center">
        <div className="search-bar">
          <SearchIcon className="search-bar_icon"/>
          <input
            type="text"
            placeholder="Search for friends post a video"
            className="search-bar_input"
          />
        </div>
      </div>
      <div className="navbar-container_right">
        <div className="navbar-links">
          <span className="navbar-links_item" onClick={navigateHome}>Home</span>
          <span className="navbar-links_item">Timeline</span>
        </div>
        <div className="navbar-icon">
          <div className="navbar-icon_item">
            <PersonIcon />
            <span className="navbar-icon_item__badge">{addFriendCount}</span>
          </div>
          <div className="navbar-icon_item">
            <ChatBubbleIcon />
            <span className="navbar-icon_item__badge">{messageCount}</span>
          </div>
          <div className="navbar-icon_item">
            <NotificationsIcon />
            <span className="navbar-icon_item__badge">{notificationCount}</span>
          </div>
        </div>
        <img
          src={picture}
          alt="user"
          className="navbar-img"
          onClick={navigateProfile}
        />
      </div>
    </div>
  );
}

export default Navbar;
