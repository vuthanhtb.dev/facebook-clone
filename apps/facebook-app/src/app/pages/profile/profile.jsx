import { Navbar, Sidebar, Feed, Rightbar } from '@app/components';
import { currentUser, friends, posts, friendsClose } from '@app/fe-data';
import './profile.scss';

const Profile = () => {
  const { picture, name, background } = currentUser;
  return (
    <div className="profile">
      <Navbar currentUser={currentUser} />
      <div className="profile-wrapper">
        <Sidebar friends={friends} currentUser={currentUser}/>
        <div className="profile-right">
          <div className="profile-right_top">
            <div className="profile-right_top__cover">
              <img
                src={background}
                alt="background"
                className="profile-right_top__cover__background"
              />
              <img
                src={picture}
                alt="user"
                className="profile-right_top__cover__user"
              />
            </div>
            <div className="profile-right_top__info">
              <h4 className="profile-right_top__info_name">{name}</h4>
              <span className="profile-right_top__info_desc">Hi Friends!</span>
            </div>
          </div>
          <div className="profile-right_bottom">
            <Feed friends={friends} posts={posts} currentUser={currentUser}/>
            <Rightbar currentUser={currentUser} friendsClose={friendsClose} isProfile/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profile;
