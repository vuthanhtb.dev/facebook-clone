import { useNavigate } from 'react-router-dom';
import './login.scss';

const Login = () => {
  const navigate = useNavigate();
  const navigateRegister = () => navigate('/register');
  const navigateHome = () => navigate('/');

  const onHandeSubmit = (event) => {
    event.preventDefault();
    navigateHome();
  }

  return (
    <div className="login">
      <div className="login-wrapper">
        <div className="login-left">
          <h3 className="login-left_logo">Facebook</h3>
          <span className="login-left-desc">
            Connect with friends and the world around you on Facebook
          </span>
        </div>

        <div className="login-right">
          <div className="login-right_box">
            <div className="login-right_box__bottom">
              <form className="login-right_box__bottom__form" onSubmit={onHandeSubmit}>
                <input
                  type="email"
                  id="email"
                  placeholder="Email"
                  className="login-right_box__bottom__form__login-input"
                  required
                />
                <input
                  type="password"
                  id="password"
                  placeholder="Password"
                  className="login-right_box__bottom__form__login-input"
                  required
                />
                <button type="submit" className="login-right_box__bottom__form__login">Login</button>
                <button className="login-right_box__bottom__form__register" onClick={navigateRegister}>Register Account</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
