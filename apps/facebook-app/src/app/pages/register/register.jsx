import { useNavigate } from 'react-router-dom';
import { DriveFolderUploadOutlined } from '@mui/icons-material';
import './register.scss';

const Register = () => {
  const navigate = useNavigate();
  const navigateLogin = () => navigate('/login');

  return (
    <div className="register">
      <div className="register-wrapper">
        <div className="register-left">
          <h3 className="register-left_logo">Facebook</h3>
          <span className="register-left-desc">
            Connect with friends and the world around you on Facebook
          </span>
        </div>
        <div className="register-right">
          <div className="register-right_box">
            <div className="register-right_box__top">
              <img
                src="https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg"
                alt="profile"
                className="register-right_box__top__img"
              />
              <div className="form-input">
                    <label htmlFor="file">
                      Image: <DriveFolderUploadOutlined className="icon"/>
                    </label>
                    <input
                      type="file"
                      name="file"
                      id="file"
                      accept=".png,.jpg,.jpeg"
                      style={{ display: 'none' }}
                    />
                  </div>
            </div>
            <div className="register-right_box__bottom">
              <form className="register-right_box__bottom__form">
                <input
                  type="text"
                  id="username"
                  placeholder="Username"
                  className="register-right_box__bottom__form__register-input"
                  required
                />
                <input
                  type="email"
                  id="email"
                  placeholder="Email"
                  className="register-right_box__bottom__form__register-input"
                  required
                />
                <input
                  type="password"
                  id="password"
                  placeholder="Password"
                  className="register-right_box__bottom__form__register-input"
                  required
                />
                <input
                  type="password"
                  id="confirmPassword"
                  placeholder="Confirm Password"
                  className="register-right_box__bottom__form__register-input"
                  required
                />
                <button type="submit" className="register-right_box__bottom__form__register">Register</button>
                <button className="register-right_box__bottom__form__login" onClick={navigateLogin}>Login Account</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;
