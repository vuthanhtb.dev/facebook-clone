import { Navbar, Sidebar, Feed, Rightbar } from '@app/components';
import { currentUser, friendsOnline, friends, posts } from '@app/fe-data';
import './home.scss';

const Home = () => {
  return (
    <div className="home-container">
      <Navbar currentUser={currentUser}/>
      <div className="home-wrapper">
        <Sidebar friends={friends} currentUser={currentUser}/>
        <Feed friends={friends} posts={posts} currentUser={currentUser}/>
        <Rightbar friendsOnline={friendsOnline}/>
      </div>
    </div>
  );
}

export default Home;
