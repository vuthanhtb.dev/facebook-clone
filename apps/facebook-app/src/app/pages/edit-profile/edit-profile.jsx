import { Navbar, Sidebar } from '@app/components';
import { currentUser, friends } from '@app/fe-data';
import { DriveFolderUploadOutlined } from '@mui/icons-material';
import './edit-profile.scss';

const EditProfile = () => {
  const {
    background,
    picture,
    name,
    username,
    email,
    phoneNumber,
    address,
    country
  } = currentUser;
  return (
    <div className="edit-profile">
      <Navbar currentUser={currentUser} />
      <div className="edit-profile-wrapper">
        <Sidebar friends={friends} currentUser={currentUser}/>
        <div className="edit-profile-info">
          <div className="edit-profile-info_top">
            <div className="edit-profile-info_top__cover">
              <img
                src={background}
                alt="background"
                className="edit-profile-info_top__cover__background"
              />
              <img
                src={picture}
                alt="user"
                className="edit-profile-info_top__cover__user"
              />
            </div>
            <div className="edit-profile-info_top__info">
              <h4 className="edit-profile-info_top__info_name">{name}</h4>
              <span className="edit-profile-info_top__info_desc">Hi Friends!</span>
            </div>
          </div>
          <div className="edit-profile-info_bottom">
            <div className="top"><h1>Edit User Info</h1></div>
            <div className="bottom">
              <div className="left">
                <img src={picture} alt="default-profile" />
              </div>
              <div className="right">
                <form>
                  <div className="form-input">
                    <label htmlFor="file">
                      Image: <DriveFolderUploadOutlined className="icon"/>
                    </label>
                    <input type="file" id="file" style={{ display: 'none' }}/>
                  </div>
                  <div className="form-input">
                    <label>Name</label>
                    <input type="text" placeholder="Enter name" value={name}/>
                  </div>
                  <div className="form-input">
                    <label>Username</label>
                    <input type="text" placeholder="Enter username" value={username}/>
                  </div>
                  <div className="form-input">
                    <label>Email</label>
                    <input type="text" placeholder="Enter email" value={email}/>
                  </div>
                  <div className="form-input">
                    <label>Phone Number</label>
                    <input type="text" placeholder="Enter phone number" value={phoneNumber}/>
                  </div>
                  <div className="form-input">
                    <label>Addresss</label>
                    <input type="text" placeholder="Enter address" value={address}/>
                  </div>
                  <div className="form-input">
                    <label>Country</label>
                    <input type="text" placeholder="Enter country" value={country}/>
                  </div>
                  <button type="submit" className="btn-update">Update Profile</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default EditProfile;
