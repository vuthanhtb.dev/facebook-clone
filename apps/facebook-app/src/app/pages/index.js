export { default as Home } from './home/home';
export { default as Profile } from './profile/profile';
export { default as EditProfile } from './edit-profile/edit-profile';
export { default as Register } from './register/register';
export { default as Login } from './login/login';
