export const currentUser = {
  id: 1,
  name: 'Tiểu Màn Thầu',
  username: 'tieumanthau',
  email: 'tieumanthau@mail.com',
  phoneNumber: '0989.999.999',
  address: 'Hà Nội',
  country: 'Việt Nam',
  relationship: 'Single',
  messageCount: 7,
  notificationCount: 3,
  addFriendCount: 2,
  picture: 'https://afamilycdn.com/2019/11/11/56403681101570180696959673201652704736706560o-15734639663341099305250.jpg',
  background: 'https://img5.thuthuatphanmem.vn/uploads/2021/08/25/background-3d-4k_085529380.jpg'
};

export const friends = [
  {
    id: 1,
    name: 'Triệu Lệ Dĩnh',
    username: 'dinhtl',
    email: 'dinhtl@email.com',
    profilePicture: 'https://static1.dienanh.net/upload/202207/f87ffb09-e84c-4a36-ade8-ebff08229f72.jpg'
  },
  {
    id: 2,
    name: 'Dương Mịch',
    username: 'duongmich',
    email: 'duongmich@email.com',
    profilePicture: 'https://cdn.phunuvagiadinh.vn/minhthu/auto/24_6_2022/1-2022-06-24-15-52.jpg'
  },
  {
    id: 3,
    name: 'Ngu Thư Hân',
    username: 'hannt',
    email: 'hannt@email.com',
    profilePicture: 'https://ss-images.saostar.vn/w800/pc/1663780546734/saostar-u5m0t7t250q0zbv1.jpg'
  },
  {
    id: 4,
    name: 'Triệu Lộ Tư',
    username: 'tutl',
    email: 'ntutl@email.com',
    profilePicture: 'https://i.bloganchoi.com/bloganchoi.com/wp-content/uploads/2020/10/trieu-tu-696x1044.jpg'
  },
  {
    id: 5,
    name: 'Cúc Tịnh Y',
    username: 'yct',
    email: 'yct@email.com',
    profilePicture: 'https://upload.wikimedia.org/wikipedia/commons/c/cd/Jujingyi-_C%C3%BAc_T%E1%BB%B7.jpg'
  },
  {
    id: 6,
    name: 'Lê Nguyễn Bảo Ngọc',
    username: 'baongoc',
    email: 'baongoc@email.com',
    profilePicture: 'https://photo-cms-plo.epicdn.me/w850/Uploaded/2022/vocgmvbg/2022_08_05/bao-ngoc-9019.png'
  },
  {
    id: 7,
    name: 'Tống Thiến',
    username: 'tongthien',
    email: 'tongthien@email.com',
    profilePicture: 'https://afamilycdn.com/150157425591193600/2021/8/5/2299613723267714124824148181443759168901323n-162814336493922294173.jpg'
  },
];

export const posts = [
  {
    id: 1,
    userId: 1,
    username: 'Triệu Lệ Dĩnh',
    profile: 'https://static1.dienanh.net/upload/202207/f87ffb09-e84c-4a36-ade8-ebff08229f72.jpg',
    title: '',
    body: 'Tất cả những sự khó khăn thường là để chuẩn bị cho những người bình thường một số phận phi thường - C.S. Lewis',
    photo: 'https://seotct.com/wp-content/uploads/2022/02/my-nhan-dep-nhat-trung-quoc-8.jpg',
    date: '2 phút',
    like: 500,
    comment: 900
  },
  {
    id: 2,
    userId: 1,
    username: 'Triệu Lệ Dĩnh',
    profile: 'https://static1.dienanh.net/upload/202207/f87ffb09-e84c-4a36-ade8-ebff08229f72.jpg',
    title: '',
    body: 'Chúng ta có thể gặp nhiều thất bại nhưng chúng ta không được bị đánh bại - Maya Angelou',
    photo: 'https://photo-cms-plo.epicdn.me/w850/Uploaded/2022/wpdhnwcaj/2022_08_12/nam-em-7135.jpeg',
    date: '2 phút',
    like: 500,
    comment: 900
  },
  {
    id: 3,
    userId: 7,
    username: 'Tống Thiến',
    profile: 'https://afamilycdn.com/150157425591193600/2021/8/5/2299613723267714124824148181443759168901323n-162814336493922294173.jpg',
    title: '',
    body: 'Nếu bạn muốn trở nên kỳ quặc khác người, hãy tự tin khi làm điều đó (If you\'re going to be weird, be confident about it) - Matt Hogan',
    photo: 'https://photo-cms-plo.epicdn.me/w850/Uploaded/2022/wpdhnwcaj/2022_08_12/khanh-my-nguoi-dep-bien-7872.jpeg',
    date: '2 phút',
    like: 500,
    comment: 900
  },
  {
    id: 4,
    userId: 7,
    username: 'Tống Thiến',
    profile: 'https://afamilycdn.com/150157425591193600/2021/8/5/2299613723267714124824148181443759168901323n-162814336493922294173.jpg',
    title: '',
    body: 'Bí mật của cuộc sống là ngã bảy lần và đứng dậy tám lần - trích Nhà Giả Kim | Paulo Coelho',
    photo: 'https://afamilycdn.com/150157425591193600/2021/8/5/2299613723267714124824148181443759168901323n-162814336493922294173.jpg',
    date: '2 phút',
    like: 500,
    comment: 900
  },
  {
    id: 5,
    userId: 6,
    username: 'Lê Nguyễn Bảo Ngọc',
    profile: 'https://photo-cms-plo.epicdn.me/w850/Uploaded/2022/vocgmvbg/2022_08_05/bao-ngoc-9019.png',
    title: '',
    body: 'Cách tốt nhất để cổ vũ bản thân là cố gắng cổ vũ người khác - Mark Twain',
    photo: 'https://photo-cms-plo.epicdn.me/w850/Uploaded/2022/vocgmvbg/2022_08_05/bao-ngoc-9019.png',
    date: '2 phút',
    like: 500,
    comment: 900
  },
  {
    id: 6,
    userId: 3,
    username: 'Ngu Thư Hân',
    profile: 'https://ss-images.saostar.vn/w800/pc/1663780546734/saostar-u5m0t7t250q0zbv1.jpg',
    title: '',
    body: 'Mọi người sẽ quên những gì bạn nói, quên những gì bạn đã làm, nhưng họ sẽ không bao giờ quên cảm xúc mà bạn mang lại cho họ - Maya Angelou.',
    photo: 'https://ss-images.saostar.vn/w800/pc/1663780546734/saostar-u5m0t7t250q0zbv1.jpg',
    date: '2 phút',
    like: 500,
    comment: 900
  },
  {
    id: 7,
    userId: 2,
    username: 'Dương Mịch',
    profile: 'https://cdn.phunuvagiadinh.vn/minhthu/auto/24_6_2022/1-2022-06-24-15-52.jpg',
    title: '',
    body: 'Mục đích chính của chúng ta trong cuộc sống này là giúp đỡ người khác. Và nếu bạn không thể giúp họ, ít nhất đừng làm họ bị thương - Đức Đạt Lai Lạt Ma XIV',
    photo: 'https://cdn.phunuvagiadinh.vn/minhthu/auto/24_6_2022/1-2022-06-24-15-52.jpg',
    date: '2 phút',
    like: 500,
    comment: 900
  },
  {
    id: 8,
    userId: 4,
    username: 'Triệu Lộ Tư',
    profile: 'https://i.bloganchoi.com/bloganchoi.com/wp-content/uploads/2020/10/trieu-tu-696x1044.jpg',
    title: '',
    body: 'Trong đời người có ba điều quan trọng: thứ nhất là sống tử tế, thứ hai là tử tế, và thứ ba là phải tử tế - Henry James',
    photo: 'https://i.bloganchoi.com/bloganchoi.com/wp-content/uploads/2020/10/trieu-tu-696x1044.jpg',
    date: '2 phút',
    like: 500,
    comment: 900
  },
  {
    id: 9,
    userId: 5,
    username: 'Cúc Tịnh Y',
    profile: 'https://upload.wikimedia.org/wikipedia/commons/c/cd/Jujingyi-_C%C3%BAc_T%E1%BB%B7.jpg',
    title: '',
    body: 'Vết thương không lành theo cách chúng ta muốn, chúng lành theo cách chúng cần. Cần có thời gian để vết thương mờ dần thành sẹo. Cần có thời gian để quá trình chữa bệnh diễn ra. Hãy cho bản thân thời gian đó. Hãy nhẹ nhàng với vết thương của bạn. Hãy nhẹ nhàng với trái tim của bạn. Bạn xứng đáng được chữa lành - Dele Olanubi',
    photo: 'https://upload.wikimedia.org/wikipedia/commons/c/cd/Jujingyi-_C%C3%BAc_T%E1%BB%B7.jpg',
    date: '2 phút',
    like: 500,
    comment: 900
  },
];

export const friendsOnline = [
  {
    id: 1,
    name: 'Lâm Tâm Như',
    username: 'nhult',
    email: 'nhult@mail.com',
    picture: 'https://congluan-cdn.congluan.vn/files/content/2019/09/23/lamtamnhu1-0938.jpg'
  },
  {
    id: 2,
    name: 'Kim Hee Sun',
    username: 'sunkh',
    email: 'sunkh',
    picture: 'https://images6.fanpop.com/image/photos/41700000/Kim-Hee-Sun-korean-actors-and-actresses-41795629-1920-1280.jpg'
  },
  {
    id: 3,
    name: 'Kim Tae Hee',
    username: 'heekt',
    email: 'heekt@mail.com',
    picture: 'https://photo-cms-tpo.zadn.vn/w645/Uploaded/2022/neg-sleclyr/2021_10_19/o-3750.jpg'
  },
  {
    id: 4,
    name: 'Hashimoto Kanna',
    username: 'kanna',
    email: 'kanna@mail.com',
    picture: 'https://image-us.eva.vn/upload/1-2020/images/2020-01-18/kana1-1579364311-681-width600height827.jpg'
  },
  {
    id: 5,
    name: 'Fukada Kyoko',
    username: 'kyoko',
    email: 'kyoko@mail.com',
    picture: 'http://media.tinthethao.com.vn/files/news/2016/10/10/kyoko-fukada-my-nhan-goi-cam-thu-ba-o-nhat-ban-192920.jpg'
  },
  {
    id: 6,
    name: 'Imada Mio',
    username: 'mio',
    email: 'mio@mail.com',
    picture: 'https://znews-photo.zingcdn.me/w660/Uploaded/bpivpjbp/2018_10_01/DoWPaIQVsAAblUS_1.jpg'
  },
  {
    id: 7,
    name: 'Fukuhara Haruka',
    username: 'haruka',
    email: 'haruka@mail.com',
    picture: 'https://1.bp.blogspot.com/-YVZrg4pdhmo/YF3n3kJ45UI/AAAAAAAHbTQ/k9bbF4244D4XSq9vkxTHtzWk4RP2NUkhACLcBGAsYHQ/s1055/1b.jpg'
  },
];

export const friendsClose = [
  {
    id: 1,
    name: 'Yuliya Kimura',
    username: 'kimura',
    email: 'kimura@email.com',
    profilePicture: 'https://vnn-imgs-a1.vgcloud.vn/znews-photo.zadn.vn/w660/Uploaded/ofh_btgazspf/2019_09_11/15043595_1718405131812935_5318281483775901696_n.jpg'
  },
  {
    id: 2,
    name: 'Dương Mịch',
    username: 'duongmich',
    email: 'duongmich@email.com',
    profilePicture: 'https://cdn.phunuvagiadinh.vn/minhthu/auto/24_6_2022/1-2022-06-24-15-52.jpg'
  },
  {
    id: 3,
    name: 'Ngu Thư Hân',
    username: 'hannt',
    email: 'hannt@email.com',
    profilePicture: 'https://ss-images.saostar.vn/w800/pc/1663780546734/saostar-u5m0t7t250q0zbv1.jpg'
  },
  {
    id: 4,
    name: 'Triệu Lộ Tư',
    username: 'tutl',
    email: 'ntutl@email.com',
    profilePicture: 'https://i.bloganchoi.com/bloganchoi.com/wp-content/uploads/2020/10/trieu-tu-696x1044.jpg'
  },
  {
    id: 5,
    name: 'Cúc Tịnh Y',
    username: 'yct',
    email: 'yct@email.com',
    profilePicture: 'https://upload.wikimedia.org/wikipedia/commons/c/cd/Jujingyi-_C%C3%BAc_T%E1%BB%B7.jpg'
  },
  {
    id: 6,
    name: 'Đỗ Linh Chi',
    username: 'chidl',
    email: 'chidl@email.com',
    profilePicture: 'https://photo-cms-plo.epicdn.me/w850/Uploaded/2022/wpdhnwcaj/2022_08_12/do-linh-chi-1935.jpeg'
  }
];
